import os

from flask import Flask, request
from flask_iniconfig import INIConfig
from flask_security import SQLAlchemyUserDatastore
from flask_babel import Babel
from flask_admin.contrib.sqla import ModelView
from martin.models import  security, admin, db
from martin.models.users import Role, Users
from martin.controllers.index import IndexBlueprint


def make_app(*args, **kwargs):
    app = Flask(__name__,
                template_folder='templates',
                static_folder='static',
                static_url_path='')
    INIConfig(app)
    app.config['APP_PATH'] = os.path.dirname(
        os.path.abspath(__file__))
    app.config['CFG_PATH'] = os.path.join(
        app.config['APP_PATH'], 'configurations', 'local.ini')
    app.config.from_inifile_sections(app.config['CFG_PATH'],
                                     ['app:flask'])
    db.init_app(app)
    user_datastore = SQLAlchemyUserDatastore(db, Users, Role)
    security.init_app(app, user_datastore)
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        #print request.accept_languages.best_match('en')
        #return request.accept_languages.best_match('en')
        print(type(request.accept_languages.best_match('en', 'ru')))
        return 'en'

    admin.init_app(app)
    admin.add_view(ModelView(Users, db.session))

    app.register_blueprint(IndexBlueprint)
    return app
